MBDataSpoof {
    var <hostname;
    var <port;
    var <id;
    var <task;
    var <x = 0;
    var <y = 0;
    var <z = 0;
    var <>deviation = 1;
    var pattern;
    var netAddr;
    var mbValues;
    var probs;

    *new {
        arg hostname="127.0.0.1", port=57120, id=9;
        ^super.newCopyArgs(hostname, port, id).init;
    }

    init {
        mbValues = Array.series(101, 0.4666, 0.000667);
        netAddr = NetAddr.new(hostname, port);
        pattern = nil ! 3;
        this.prCreatePatterns;
        this.prCreateTask;
    }

    prCreateTask {
        task = TaskProxy.new({
            inf.do({
                x = pattern[0].next;
                y = pattern[1].next;
                z = pattern[2].next;
                netAddr.sendMsg("/minibee/data", id, x, y, z);
                20.reciprocal.wait;
            })
        }).play;
    }

    prCreatePatterns {
        3.do ({
            arg i;
            pattern[i] = Pwalk(
                mbValues, 
                Pgauss(0.0, deviation, inf),
                Pseq([1, -1], inf),
            ).asStream;
        })
    }
    
    free {
        task.stop;
    }
}
