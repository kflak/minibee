MBData {
    classvar <>resamplingFreq = 20;
    classvar <>oscOutAddress;

    var <>minibeeID;
    var <>windowSize;
    var buffer;
    var <delta, <x=0.0, <y=0.0, <z=0.0, <xdir, <ydir, <movingAverage;
    var rawData, data, dataMul=15, dataOffset=7.0;
    var prevData;
    var runningSum=0;
    var task;
    var oscFunc;
    var <>sendOscOut = false;
    var <>sendOscOutRaw = true;
    var <>server;
    var <xbus, <ybus, <zbus, <deltabus;
    var <>trace = false;

    *new { arg minibeeID=10, windowSize=50;
        ^super.newCopyArgs(minibeeID, windowSize).init;
    }

    init {
        server = server ? Server.default;
        data = 0.0 ! 3;
        rawData = 0.0 ! 3;
        prevData = 0.0 ! 3;
        if(oscOutAddress.isNil){
            oscOutAddress = NetAddr("localhost", 12345)
        };
        buffer = List.newClear(windowSize).fill(0);
        xbus = Bus.control(server, 1);
        ybus = Bus.control(server, 1);
        zbus = Bus.control(server, 1);
        deltabus = Bus.control(server, 1);
        this.createOscFunc;
        this.createTask;
    }

    createOscFunc {
        oscFunc = OSCFunc({|oscdata|
            rawData = oscdata[2..];
            data = rawData * dataMul - dataOffset;
            data = data.clip(0.0, 1.0);
            x = data[0];
            y = data[1];
            z = data[2];
        }, '/minibee/data', argTemplate: [minibeeID]);
    }

    sendOSC {
        if(sendOscOut){
            oscOutAddress.sendMsg("/minibee/data", minibeeID, x, y, z, delta, movingAverage);
        }
    }

    sendOSCRaw {
        if(sendOscOutRaw){
            oscOutAddress.sendMsg("/minibee/data", minibeeID, rawData[0], rawData[1], rawData[2]);
        }
    }

    createTask {
        task = TaskProxy.new({
            inf.do {
                this.calcXdir;
                this.calcYdir;
                this.calcDelta;
                this.postValues;
                this.calcMovingAverage;
                this.sendOSC;
                this.sendOSCRaw;
                this.setBusses;
                resamplingFreq.reciprocal.wait;
            }
        }).play;
    }

    calcDelta {
        delta = (data - prevData).abs.sum/3;
        prevData = data.copy;
        ^delta;
    }

    calcXdir {
        xdir = data[0] - prevData[0];
        if (xdir.isPositive){ 
            xdir = 1;
        }{
            xdir = -1;
        };
    }

    calcYdir {
        ydir = data[1] - prevData[1];
        if (ydir.isPositive){ 
            ydir = 1;
        }{
            ydir = -1;
        };
    }

    setBusses {
        deltabus.set(delta);
        xbus.set(x);
        ybus.set(y);
        zbus.set(z);
    }
    
    calcMovingAverage {
        buffer.addFirst(delta);
        runningSum = runningSum + buffer[0];
        runningSum = runningSum - buffer[buffer.size - 1];
        buffer.pop;
        movingAverage = runningSum / buffer.size;
    } 

    postValues {
        if(trace){
            postf("id:%\tx:%\ty:%\tz:%\td:%\n", minibeeID, x, y, z, delta)
        }
    }
} 
