MiniBeeTest1 : UnitTest {
	test_check_classname {
		var result = MiniBee.new;
		this.assert(result.class == MiniBee);
	}
}


MiniBeeTester {
	*new {
		^super.new.init();
	}

	init {
		MiniBeeTest1.run;
	}
}
