# MiniBee

### Working with MiniBee sensors in SuperCollider

This quark contains utility classes for working with
[MiniBee](https://sensestage.eu/minibee/) sensors' built-in
accelerometers. The two main classes are `MBData` and `MBDeltaTrig`.

### MBData

`MBData` takes the incoming xyz data from a MiniBee, scales
it to the [0..1] range and makes it available through instance
variables. Additionally it calculates the amount of movement energy of a
sensor as the difference between the current xyz values and the previous
xyz values and makes it available through the `delta` variable.

```supercollider
// given 4 MiniBees with ids 1-4 
~mbIDs = (1..4);
~mbData = IdentityDictionary.new; 
~mbIDs.do{|id| ~mbData.put(id, MBData.new(id))};
// check the data of the first MiniBee:
~mbData[1].x.postln;
~mbData[1].y.postln;
~mbData[1].z.postln;
~mbData[1].delta.postln;
```

### MBDeltaTrig

```supercollider 
// MbDeltaTrig needs a Dictionary of MBData objects to do its thing,
// one for each minibee.
MBDeltaTrig.mbData = ~mbData; 
```

### Installation

Open up SuperCollider and evaluate the following line of code:
`Quarks.install("https://gitlab.com/minibee/minibee")`
